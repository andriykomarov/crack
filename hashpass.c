#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
#include "md5.h"

int main(int argc, char *argv[]) {
    
    FILE *f;
    FILE *f1;
    char *str = malloc(100);
    char *hash = malloc(100);
    
    f = fopen(argv[1], "r");
    
    if (!f) {
        
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
        
    }
    
    f1 = fopen(argv[2], "w");
    
     if (!f1) {
        
        printf("Can't open %s for reading\n", argv[2]);
        exit(1);
        
    }
    
    while(!feof(f)) {
        
        fgets(str, (strlen(argv[1])), f);
        hash = md5(str, strlen(str));
        fprintf(f1, "%s\n", hash);
        
    }
    
    free(hash);
    free(str);
    fclose(f);
    fclose(f1);
    
}